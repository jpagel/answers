# README #

This is a skeleton Symfony 2 app that I spent a day putting together for a job interview.

* You can see the list of all answers at /api/list
* You can search answer titles at eg /api/list?search=how%20to (which will return all titles beginning "how to")
* You can view an individual answer with its comments at /api/answer/{id} where id can be the numeric id or the slug
* there are 2 testviews for demonstrating the post methods: /testview/answer/{id} allows you to post a comment and /testview/list allows you to post a new answer
* the carpark report is at /api/cars
* see the routes in /app/config/routing.yml

## SETUP ##
This is a standard Symfony 2 install. The dependencies are in composer.json. There is a small amount of sample data in /src/AppBundle/DataFixtures/ORM/LoadSampleData.php. You can load it using a doctrine command:
`php app/console doctrine:fixtures:load`

The entity classes are in src/AppBundle/Entity, and the ORM configuration is in src/AppBundle/Resources/config/doctrine/

## UNFINISHED ##
The spec required full search and file uploads. I have not done this (although the doctrine entities for attachments are there). Given world enough and time I would have written some phpspecs for my EntityWrangler library and some behat tests to verify the api output.