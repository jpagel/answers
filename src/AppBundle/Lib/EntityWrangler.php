<?php
namespace AppBundle\Lib;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Comment;

class EntityWrangler {
    public static function makeAnswerListArray($answers) {
        $list = array();
        foreach($answers as $answer) {
            $list[] = array(
                'title' => $answer->getTitle(),
                'createdat' => $answer->getCreatedAt(),
                'url' => self::makeAnswerUrl($answer)
            );
        }
        return $list;
    }

    public static function makeSingleAnswerArray($answer, $comments) {
        $comments_array = array();
        foreach($comments as $comment) {
            $comments_array[] = self::makeSingleCommentArray($comment);
        }
        return array(
            'id' => $answer->getId(),
            'title' => $answer->getTitle(),
            'description' => $answer->getDescription(),
            'content' => $answer->getContent(),
            'user_email' => $answer->getUserEmail(),
            'createdat' => $answer->getCreatedAt(),
            'slug' => $answer->getSlug(),
            'comments' => $comments_array
        );
    }

    protected static function makeSingleCommentArray($comment) {
        return array(
            'email' => $comment->getUserEmail(),
            'content' => $comment->getContent(),
            'createdat' => $comment->getCreatedat()
        );
    }
    
    public static function makeAnswerUrl(Answer $answer) {
        return "/api/answer/{$answer->getSlug()}";
    }

    public static function newAnswerFromArray($data) {
        $answer = new Answer();
        $answer->setTitle($data['title']);
        $answer->setUserEmail($data['user_email']);
        $answer->setDescription($data['description']);
        $answer->setContent($data['content']);
        $answer->setSlug(self::sluggify($data['title']));
        $answer->setCreatedat(new \DateTime());
        return $answer;
    }

    public static function newCommentFromArray($data) {
        $comment = new Comment();
        $comment->setContent($data['content']);
        $comment->setUserEmail($data['user_email']);
        $comment->setCreatedat(new \DateTime());
        return $comment;
    }

    public static function sluggify($s) {
        return substr(preg_replace('/[^a-zA-Z0-9]/', '', $s),0, 50);
    }

}
