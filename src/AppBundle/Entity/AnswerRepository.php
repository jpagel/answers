<?php

namespace AppBundle\Entity;

class AnswerRepository extends \Doctrine\ORM\EntityRepository {

    public function findAnswers($searchterm) {
        $rst = $this->getEntityManager();
        if($searchterm) {
            $query = "SELECT a FROM AppBundle:Answer a WHERE a.title LIKE :searchterm";
            $rst = $rst->createQuery($query)
                ->setParameter('searchterm', $searchterm . "%");
        } else {
            $query = 'SELECT a FROM AppBundle:Answer a ORDER BY a.title';
            $rst = $rst->createQuery($query);
        }
        return $rst->getResult();
    }

}
