<?php
namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Answer;
use AppBundle\Entity\Comment;

class LoadSampleData implements FixtureInterface {
    public function load(ObjectManager $em) {
        $answers = array(
            array(
                "title" => "How to cook a rabbit",
                "description" => "a way to cook a rabbit",
                "slug" => "howtocookarabbit",
                "user_email" => "beeton@cherry.drop",
                "content" => "First catch a rabbit. Then cook it."
            ),
            array(
                "title" => "How to buy a house",
                "description" => "a guide to buying houses",
                "slug" => "howtobuyahouse",
                "user_email" => "beanie@cherry.drop",
                "content" => "Go to a house shop (eg Homebase) and choose a nice house. Then make your fortune in finance and spend the rest of your life paying for it."
            ),
            array(
                "title" => "Where to find a piano tuner",
                "description" => "tips for locating piano tuners",
                "slug" => "wheretofindapianotuner",
                "user_email" => "beethoven@cherry.drop",
                "content" => "You could try Yellow Pages, Craig's List, Trustatraider.com or just wandering the streets and approaching homeless people."
            ),
            array(
                "title" => "Age of the universe",
                "description" => "answering the question 'How old is the universe?'",
                "slug" => "ageoftheuniverse",
                "user_email" => "dawkins@cherry.drop",
                "content" => "13.6 billion years."
            ),
        );
        foreach($answers as $answerdata) {
            $answer = new Answer();
            $answer->setTitle($answerdata["title"]);
            $answer->setDescription($answerdata["description"]);
            $answer->setSlug($answerdata["slug"]);
            $answer->setUserEmail($answerdata["user_email"]);
            $answer->setContent($answerdata["content"]);
            $answer->setCreatedat(new \DateTime());
            $em->persist($answer);
        }

        $comments = array(
            array(
                'user_email' => 'detester@cherry.drop',
                'content' => "That's no way to cook a rabbit",
                'answer_id' => 1
            )
        );
        $em->flush();
        
        foreach($comments as $commentdata) {
            $comment = new Comment();
            $comment->setUserEmail($commentdata['user_email']);
            $comment->setContent($commentdata['content']);
            $comment->setAnswerId($commentdata['answer_id']);
            $comment->setCreatedat(new \DateTime());
            $em->persist($comment);
        }
        $em->flush();
    }
}
