<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Car;
use Doctrine\ORM\EntityManager;
use AppBundle\Lib\EntityWrangler;

class CarController {

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function reportAction() {

        $dql = "SELECT c FROM car";
        $query = $this->em->createQuery($dql);
        
        $data = array('msg'=>'hello');
        return new JsonResponse($data);
    }
}
