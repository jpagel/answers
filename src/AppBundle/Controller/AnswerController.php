<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Answer;
use Doctrine\ORM\EntityManager;
use AppBundle\Lib\EntityWrangler;

class AnswerController {

    public function __construct(EntityManager $em, EntityWrangler $ew) {
        $this->em = $em;
        $this->ew = $ew;
    }

    public function listAction(Request $req)
    {
        $searchterm = $req->get('search');
        $answers = $this->em->getRepository('AppBundle:Answer')->findAnswers($searchterm);
        $data = $this->ew->makeAnswerListArray($answers);
        return new JsonResponse($data);
    }

    public function answerAction(Request $req, $id) {
        if(is_numeric($id)) {
            $answers = array($this->em->getRepository('AppBundle:Answer')->find($id));
        } else {
            $answers = $this->em->getRepository('AppBundle:Answer')->findBySlug($id);
        }
        if(!count($answers)) {
            throw $this->createNotFoundException(
                'No answer found for id ' . $id
            );
        }
        $answer = $answers[0];
        $comments = $answer->getComments();
foreach($comments as $comment) {}

        $data = $this->ew->makeSingleAnswerArray($answer, $comments);

        return new JsonResponse($data);
    }

    public function createAction(Request $req) {
        $data = array(
            'title' => $req->get('title'),
            'user_email' => $req->get('user_email'),
            'description' => $req->get('description'),
            'content' => $req->get('content')
        );
        $answer = $this->ew->newAnswerFromArray($data);
        $this->em->persist($answer);
        $this->em->flush();

        $answers = $this->em->getRepository('AppBundle:Answer')->findAnswers(null);
        $data = $this->ew->makeAnswerListArray($answers);
        return new JsonResponse($data);
    }

}
