<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Answer;
use Doctrine\ORM\EntityManager;
use AppBundle\Lib\EntityWrangler;

class CommentController {

    public function __construct(EntityManager $em, EntityWrangler $ew) {
        $this->em = $em;
        $this->ew = $ew;
    }

    public function createAction(Request $req) {
        $data = array(
            'content' => $req->get('content'),
            'user_email' => $req->get('user_email'),
        );
        $comment = $this->ew->newCommentFromArray($data);
        $answer = $this->em->getRepository('AppBundle:Answer')->find($req->get('answer_id'));
        $comment->setAnswer($answer);

        $this->em->persist($comment);
        $this->em->flush();
        $output = array(
            'status' => 'success',
            'message' => 'comment was saved'
        );
        return new JsonResponse($output);
    }
}
