<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\Answer;
use Doctrine\ORM\EntityManager;
use AppBundle\Lib\EntityWrangler;

class TestviewAnswerController {

    public function __construct(EntityManager $em, EntityWrangler $ew, $templating) {
        $this->em = $em;
        $this->ew = $ew;
        $this->templating = $templating;
    }

    public function listAction(Request $req)
    {
        $searchterm = $req->get('search');
        $answers = $this->em->getRepository('AppBundle:Answer')->findAnswers($searchterm);
        $data = $this->ew->makeAnswerListArray($answers);
        $html = $this->templating->render(
            'list.html.twig',
            array('answers' => $data, 'randname' => uniqid())
        );
        return new Response($html);
    }

    public function answerAction(Request $req, $id) {
        if(is_numeric($id)) {
            $answers = array($this->em->getRepository('AppBundle:Answer')->find($id));
        } else {
            $answers = $this->em->getRepository('AppBundle:Answer')->findBySlug($id);
        }
        if(!count($answers)) {
            throw $this->createNotFoundException(
                'No answer found for id ' . $id
            );
        }
        $answer = array_shift($answers);
        $comments = $answer->getComments();
        $data = $this->ew->makeSingleAnswerArray($answer, $comments);

        $html = $this->templating->render(
            'answer.html.twig',
            array(
                'answer' => $data,
                'randname' => uniqid()
            )
        );
        return new Response($html);
    }
}
